[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](./LICENSE)  


# Usage 

The purpose of this repo is to help replicate my current Ubuntu workstation on any new system. The scripts in this repo are my own and free to use. All other software mentioned in this project come either from the Ubuntu repos or from their respective creators. Links to any additional software, tools, or extensions are provided below.


## Installation 

Here are the instructions to rebuild the same Ubuntu system that I run on my main workstation. All the installation scripts can be found in the [scripts](./scripts/) directory. Make sure to make scripts executable prior to running any commands. 

To make all scripts executable, open a terminal from the root folder of this project and run the following command: 

* `chmod a+x scripts/*.sh`


### Step One: Initial Install  

Locate the **initial-install** script under the _scripts_ folder and execute in a terminal 

* `./initial-install.sh`

Once the installation is complete, reboot the system for changes to take effect.


### Step Two: Post-Install

After the reboot, execute the **post-install** script in a terminal

* `./post-install.sh`

The post-install script will execute the [Flatpak](scripts/flatpak.sh) script located in the same folder and will append the contents of the [bashrc_addon](scripts/bashrc_addon.txt) file to your bashrc file (commonly located in your home directory). 


Here is a list of the Flatpak applications that will be installed:  

* [Spotify](https://flathub.org/apps/details/com.spotify.Client)
* [Discord](https://flathub.org/apps/details/com.discordapp.Discord)
* [Pycharm-Community](https://flathub.org/apps/details/com.jetbrains.PyCharm-Community)
* [Minecraft](https://flathub.org/apps/details/com.mojang.Minecraft)
* [Ferdi](https://flathub.org/apps/details/com.getferdi.Ferdi)  
* [FlatSeal](https://flathub.org/apps/details/com.github.tchx84.Flatseal)
* [OBS Studio](https://flathub.org/apps/details/com.obsproject.Studio)  
* [Kdenlive](https://flathub.org/apps/details/org.kde.kdenlive)  


### Step Three: Other Applications  

The rest of the applications I have installed on my system were either downloaded directly from the developer's site, Github, or by adding the application's PPA to Ubuntu's sources.

Here is a list of additional applications installed on my system:  

* [Android Studio](https://developer.android.com/studio)  
* [Bootstrap Studio](https://bootstrapstudio.io/download)  
* [Gitfiend](https://gitfiend.com/)  
* [Veracrypt](https://www.veracrypt.fr/en/Downloads.html)  
* [VirtualBox Extension Pack](https://www.virtualbox.org/wiki/Downloads#:~:text=VirtualBox%20Extension%20Pack)  
* [Terminal Countdown](https://github.com/antonmedv/countdown/)  


## Additonal Steps After Post-Install  

The rest of this README will focus on customizing the look and feel of your system, but is completely **optional**. Make sure you followed steps [1](#-Step-One) and [2](#-Step-Two) above before moving forward.  

The two main tools needed for customizing the desktop environment are _gnome-shell-extensions_ and _gnome-tweaks_, both of which should already be installed.  


### Gnome Extensions

The easiest way to search and install gnome extensions is to add the browser extension which you can find here: [Browser Extension](https://extensions.gnome.org/) 

Once the browser extension is installed, click on the extension icon to redirect you to the gnome extensions site. The following is a list of extensions that are currently installed on my system:  

* [ArcMenu](https://extensions.gnome.org/extension/3628/arcmenu/)
* [gTile](https://extensions.gnome.org/extension/28/gtile/)
* [OpenWeather](https://extensions.gnome.org/extension/750/openweather/)
* [Screenshot Tool](https://extensions.gnome.org/extension/1112/screenshot-tool/)
* [Transparent Top Bar](https://extensions.gnome.org/extension/3960/transparent-top-bar-adjustable-transparency/)
* [User Themes](https://extensions.gnome.org/extension/19/user-themes/)
* [Dash To Dock](https://extensions.gnome.org/extension/307/dash-to-dock/)
* [Pop-OS Shell](https://github.com/pop-os/shell)

The last two extensions from the list above require a bit more work for them to run properly on Ubuntu (see below).  


#### Dash-To-Dock  

Ubuntu comes with it's own version of a dock pre-installed that interferes with the dash-to-dock extension and can cause it not to work. In order to use dash-to-dock, you can disable the ubuntu dock by opening up "tweaks" and clicking the enable/disable slider, but the most prominent way of removing the ubuntu dock is to delete it entirely from the system. You can remove it entirely by running the following command: 

* `sudo apt remove gnome-shell-extension-ubuntu-dock`

This will successfully remove the ubuntu-dock, but it will also remove the _ubuntu-desktop_ meta package. This may be something to think about if, for example, you choose to upgrade your Ubuntu version to a new version. For more information you can check out this article: [How to Remove or Disable Ubuntu Dock](https://www.linuxuprising.com/2018/08/how-to-remove-or-disable-ubuntu-dock.html)  


#### Pop-OS Shell

Pop-OS Shell extension needs to be manually installed. You can download the repository here: [Pop-OS Shell](https://github.com/pop-os/shell). 

Follow the instructions on the project's [README](https://github.com/pop-os/shell#readme) page, then reboot to complete install.  


### Gnome Tweaks

For customizing the shell, make sure you have _gnome-tweaks_ and the [User Themes](https://extensions.gnome.org/extension/19/user-themes/) extension installed. Once installed, open gnome tweaks and click the **appearance** as shown below:  

![Gnome Tweaks](screenshots/gnome-tweaks.png)  
**NOTE**: Make sure to copy any application and icon themes to their appropriate directories.
* For Application (GTK) Themes:  `~/.local/share/themes`  
* For Icon/Cursor Themes:  `~/.local/share/icons`  

**Current System Theme**  

* Application/Shell Theme: [Layan GTK](https://www.gnome-look.org/p/1309214/)
* Icons: [Papirus Dark](https://www.gnome-look.org/p/1166289/)
* Cursor: [Bibata Ice](https://www.gnome-look.org/p/1197198/)
* Wallpaper: [Mirages](https://romaintrystram.myportfolio.com/mirages)

# End Result 

Once all the tools, apps, and flatpaks have been installed on your system your Desktop should look something like this:  
   
**Gnome Overview** 

![Overview](screenshots/overview-preview.png)  


**Desktop Layout** 

![Desktop](screenshots/desktop-preview.png)  


**Pop-OS Tiling Shell** 

![Pop-Os Tiling](screenshots/tiling-preview.png)  

