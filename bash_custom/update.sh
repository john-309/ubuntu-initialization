#! /bin/bash

# Look For Updates
sudo apt update

# Install Updates
sudo apt upgrade -y

# Remove Unused/Deprecated Packages
sudo apt autoremove

# Ckeck and Install Flatpak Updates
flatpak update -y

