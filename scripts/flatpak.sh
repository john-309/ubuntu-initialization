#!/bin/bash

# The following will install additional flatpak applications.

# Make sure the initial-install script has already been executed 
# before moving forward with this script.

# Flatpak Apps
# Spotify, Discord, Pycharm-Community, Minecraft Launcher, Ferdi Messaging
flatpak install flathub com.spotify.Client com.discordapp.Discord com.jetbrains.PyCharm-Community com.mojang.Minecraft com.getferdi.Ferdi com.github.tchx84.Flatseal com.obsproject.Studio org.kde.kdenlive
