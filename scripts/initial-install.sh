#!/bin/bash

# These are the initial applications and libraries needed before doing any customization to the system
sudo apt install apt-utils htop vim git gimp inkscape default-jdk python3 python3-dev python-is-python3 build-essential cmake gnome-tweaks guake menulibre vlc sl cowsay fortune neofetch nodejs npm tilix filezilla remmina virtualbox virtualbox-guest-utils godot3 steam-installer fish curl wget unzip ufw gnome-shell-extensions


# The following will install flatpak, the software plugin for the app store, 
# and the flathub repository.
# NOTE: This script only works with Ubuntu systems on version 18.10 or newer

# Install Flatpak
sudo apt install flatpak

#Install Software Flatpak Plugin
sudo apt install gnome-software-plugin-flatpak

# Add the Flathub repository
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Print message after installation is complete
printf "\n\nAll applications are now installed! \nPlease reboot for changes to take effect.\n"
