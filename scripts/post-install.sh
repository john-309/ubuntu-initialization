#!/bin/bash

# Make sure the initial-install script has already been executed 
# before moving forward with this script.


# Install Flatpak Apps
source ./flatpak.sh
printf "\nFlatpak Apps Installed!\n"

# Make update.sh executable
chmod a+x ../bash_custom/update.sh

# Copy bash_custom folder to home directory as a hidden folder
printf "\nMoving bash_custom to ~/.bash_custom..."
cp -R ../bash_custom/ ~/.bash_custom/

# Append ASCII Logo and "update-now" alias to .bashrc file
printf "\nAppending ASCII Art and update-now alias to .bashrc file...\n"
cat bashrc_addon.txt >> ~/.bashrc

printf "\nBash art and alias succesfully transferred!"
printf "\nReboot for changes to take effect."

